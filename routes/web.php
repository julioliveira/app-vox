<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'empresa'], function (){

    Route::get('/', 'EmpresaController@index');

    Route::post('/search', 'EmpresaController@search');

    Route::get('/add', 'EmpresaController@add');

    Route::post('/add', 'EmpresaController@insert');

    Route::get('/{id}', 'EmpresaController@show');

    Route::post('/{id}', 'EmpresaController@edit');

    Route::get('/delete/{id}', 'EmpresaController@delete');

});

Route::group(['prefix' => 'socio'], function (){

    Route::get('/', 'SocioController@index');

    Route::post('/search', 'SocioController@search');

    Route::get('/add', 'SocioController@add');

    Route::post('/add', 'SocioController@insert');

    Route::get('/{id}', 'SocioController@show');

    Route::post('/{id}', 'SocioController@edit');

    Route::get('/delete/{id}', 'SocioController@delete');
});

