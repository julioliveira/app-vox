
# App Vox

App Vox é uma aplicação para avaliação para ingressar como analista/programador, feito por Júlio Barbosa como aplicação teste para analise pela Vox. 

## Requisitos

- Docker instalado [link](https://www.digitalocean.com/community/tutorials/como-instalar-e-usar-o-docker-no-ubuntu-16-04-pt)
- Docker Compose instalado [link](https://www.digitalocean.com/community/tutorials/how-to-install-docker-compose-on-ubuntu-16-04)

## Instalando

- Executar o comando ```docker-compose up -d```

Projeto poderá ser acessado pelo link [http:localhost:8080](http:localhost:8080)

## Detalhes

Banco de dados já está no volume ```dbdata``` dentro da estrutura. Necessário rode os seguintes comandos adicionais
```
docker-compose exec app php artisan key:generate
docker-compose exec app php artisan optimize
```

## Rodando sem o docker

É necessário que crie um banco de dados ```db_vox``` e editar o ```.env``` com as configurações do banco de dados utilizado.

Depois rode os comandos:
```
php artisan migrate:refresh
php artisan key:generate
php artisan optimize
php artisan serve
```

O projeto poderá ser acessado pelo link [http:localhost:8000](http:localhost:8000)


### Recuperação de senha 

Para que a função de recuperação de senha funcione corretamento é necessário modificar o arquivo ```.env``` configurando com um servidor smtp correto.

```
MAIL_DRIVER=smtp
MAIL_HOST=smtp.mailtrap.io
MAIL_PORT=2525
MAIL_USERNAME=null
MAIL_PASSWORD=null
MAIL_ENCRYPTION=null
```