@extends('layouts.app')

@section('title-content', 'Cadastrar Sócio')

@section('content')

    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    <form class="form-horizontal" name="socio" method="POST" action="/socio/{{ $socio->id }}">
        {{ csrf_field() }}

        <div class="form-group{{ $errors->has('nome') ? ' has-error' : '' }}">
            <label for="name" class="col-md-4 control-label">Nome</label>

            <div class="col-md-6">
                <input id="nome" type="text" class="form-control" name="nome" value="{{ $socio->nome }}" required
                       autofocus>

                @if ($errors->has('nome'))
                    <span class="help-block">
                        <strong>{{ $errors->first('nome') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            <label for="email" class="col-md-4 control-label">E-mail</label>

            <div class="col-md-6">
                <input id="email" type="email" class="form-control" name="email" value="{{ $socio->email }}" required>

                @if ($errors->has('email'))
                    <span class="help-block">
                         <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('cpf') ? ' has-error' : '' }}">
            <label for="cpf" class="col-md-4 control-label">CPF</label>

            <div class="col-md-6">
                <input id="cpf" type="text" class="form-control" name="cpf" value="{{ $socio->cpf }}" required>

                @if ($errors->has('cpf'))
                    <span class="help-block">
                        <strong>{{ $errors->first('cpf') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('data_nascimento') ? ' has-error' : '' }}">
            <label for="data_nascimento" class="col-md-4 control-label">Data Nascimento</label>

            <div class="col-md-6">
                <input id="data_nascimento" type="date" class="form-control" name="data_nascimento" value="{{ $socio->data_nascimento }}" required>

                @if ($errors->has('data_nascimento'))
                    <span class="help-block">
                        <strong>{{ $errors->first('data_nascimento') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('telefone') ? ' has-error' : '' }}">
            <label for="telefone" class="col-md-4 control-label">Telefone</label>

            <div class="col-md-6">
                <input id="telefone" type="text" class="form-control" name="telefone" value="{{ $socio->telefone }}" required>

                @if ($errors->has('telefone'))
                    <span class="help-block">
                        <strong>{{ $errors->first('telefone') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
                <button type="submit" class="btn btn-primary">
                    Cadastrar
                </button>
                <a href="/socio" class="btn">Voltar</a>
            </div>
        </div>
    </form>

@endsection
