@extends('layouts.app')

@section('title-content', 'Sócio')

@section('content')

    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    <form class="form-inline" method="post" action="/socio/search">
        {{ csrf_field() }}
        <a href="/socio/add" class="btn btn-default">Cadastrar uma novo sócio</a>
        <div class="form-group">
            <input type="text" class="form-control" name="cpf" id="cpf" placeholder="Pesquisar por CPF">
        </div>
        <button type="submit" class="btn btn-default">Pesquisar</button>
    </form>
    <br>
    <table class="table table-hover">
        <tr>
            <th>Nome</th>
            <th>CPF</th>
            <th>E-mail</th>
            <th>Telefone</th>
            <th>Nascimento</th>
            <th>Ações</th>
        </tr>
        @foreach($socios as $socio)
        <tr>
            <td>{{$socio->nome}}</td>
            <td>{{$socio->cpf}}</td>
            <td>{{$socio->email}}</td>
            <td>{{$socio->telefone}}</td>
            <td>{{date('d/m/Y', strtotime($socio->data_nascimento))}}</td>
            <td><a href="/socio/{{$socio->id}}">Editar</a> - <a href="/socio/delete/{{$socio->id}}" onclick="return confirm('Tem certeza que deseja deletar?');">Deletar</a></td>
        </tr>
        @endforeach

    </table>

@endsection