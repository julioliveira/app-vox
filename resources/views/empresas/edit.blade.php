@extends('layouts.app')

@section('title-content', 'Cadastrar Empresa')

@section('content')

    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    <form class="form-horizontal" name="empresa" method="POST" action="/empresa/{{$empresa->id}}">
        {{ csrf_field() }}

        <div class="form-group{{ $errors->has('razao_social') ? ' has-error' : '' }}">
            <label for="name" class="col-md-4 control-label">Razão Social</label>

            <div class="col-md-6">
                <input id="razao_social" type="text" class="form-control" name="razao_social" value="{{ $empresa->razao_social }}" required
                       autofocus>

                @if ($errors->has('razao_social'))
                    <span class="help-block">
                        <strong>{{ $errors->first('razao_social') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('nome_fantasia') ? ' has-error' : '' }}">
            <label for="email" class="col-md-4 control-label">Nome Fantasia</label>

            <div class="col-md-6">
                <input id="nome_fantasia" type="text" class="form-control" name="nome_fantasia" value="{{ $empresa->nome_fantasia }}" required>

                @if ($errors->has('nome_fantasia'))
                    <span class="help-block">
                         <strong>{{ $errors->first('nome_fantasia') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('cnpj') ? ' has-error' : '' }}">
            <label for="email" class="col-md-4 control-label">CNPJ</label>

            <div class="col-md-6">
                <input id="cnpj" type="text" class="form-control" name="cnpj" value="{{ $empresa->cnpj }}" required>

                @if ($errors->has('cnpj'))
                    <span class="help-block">
                        <strong>{{ $errors->first('cnpj') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
                <button type="submit" class="btn btn-primary">
                    Cadastrar
                </button>
                <a href="/empresa" class="btn">Voltar</a>
            </div>
        </div>
    </form>

@endsection
