@extends('layouts.app')

@section('title-content', 'Empresas')

@section('content')

    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    <form class="form-inline" method="post" action="/empresa/search">
        {{ csrf_field() }}
        <a href="/empresa/add" class="btn btn-default">Cadastrar uma nova empresa</a>
        <div class="form-group">
            <input type="text" class="form-control" name="cnpj" id="cnpj" placeholder="Pesquisar por CNPJ">
        </div>
        <button type="submit" class="btn btn-default">Pesquisar</button>
    </form>
    <br>
    <table class="table table-hover">
        <tr>
            <th>Razão Social</th>
            <th>Nome Fantasia</th>
            <th>CNPJ</th>
            <th>Ações</th>
        </tr>
        @foreach($empresas as $empresa)
        <tr>
            <td>{{$empresa->razao_social}}</td>
            <td>{{$empresa->nome_fantasia}}</td>
            <td>{{$empresa->cnpj}}</td>
            <td><a href="/empresa/{{$empresa->id}}">Editar</a> - <a href="/empresa/delete/{{$empresa->id}}" onclick="return confirm('Tem certeza que deseja deletar?');">Deletar</a></td>
        </tr>
        @endforeach

    </table>

@endsection