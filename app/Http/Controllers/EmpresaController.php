<?php

namespace App\Http\Controllers;

use App\Empresa;
use Illuminate\Http\Request;

class EmpresaController extends Controller
{

    protected $model;

    public function __construct(Empresa $model)
    {
        $this->model = $model;
    }


    public function index()
    {
        $empresas = $this->model->all();
        return view('empresas.index', [
            'empresas' => $empresas
        ]);
    }

    public function add()
    {
        return view('empresas.add');
    }

    public function insert(Request $request)
    {
        $empresa = $this->model->create([
            'razao_social' => $request->input('razao_social'),
            'nome_fantasia' => $request->input('nome_fantasia'),
            'cnpj' => $request->input('cnpj')
        ]);

        return redirect()->action('EmpresaController@index')->with('status', 'Empresa cadastrada com sucesso!');
    }

    public function show($id)
    {
        $empresa = $this->model->findOrFail($id);
        return view('empresas.edit', [
            'empresa' => $empresa
        ]);
    }

    public function edit(Request $request, $id)
    {
        $result = $this->model->findOrFail($id);
        $result->update([
            'razao_social' => $request->input('razao_social'),
            'nome_fantasia' => $request->input('nome_fantasia'),
            'cnpj' => $request->input('cnpj')
        ]);

        return redirect()->action('EmpresaController@show', ['id' => $id])->with('status', 'Empresa editada com sucesso!');
    }

    public function delete($id)
    {
        $result = $this->model->where('id', '=', $id)->delete();
        return redirect()->action('EmpresaController@index')->with('status', 'Empresa deletada com sucesso!');
    }

    public function search(Request $request){

        $result = (($request->cnpj === null || $request->cnpj == '') ?$this->model->all() : $this->model->where('cnpj','like',"%$request->cnpj%")->orderBy('id')->get());
        return view('empresas.index', [
            'empresas' => $result
        ]);
    }
}
