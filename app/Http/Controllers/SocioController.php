<?php

namespace App\Http\Controllers;

use App\Socio;
use Illuminate\Http\Request;

class SocioController extends Controller
{

    protected $model;

    public function __construct(Socio $model)
    {
        $this->model = $model;
    }


    public function index()
    {
        $socios = $this->model->all();
        return view('socios.index', [
            'socios' => $socios
        ]);
    }

    public function add()
    {
        return view('socios.add');
    }

    public function insert(Request $request)
    {
        $socio = $this->model->create([
            'nome' => $request->input('nome'),
            'email' => $request->input('email'),
            'cpf' => $request->input('cpf'),
            'data_nascimento' => $request->input('data_nascimento'),
            'telefone' => $request->input('telefone')
        ]);

        return redirect()->action('SocioController@index')->with('status', 'Sócio cadastrada com sucesso!');
    }

    public function show($id)
    {
        $socio = $this->model->findOrFail($id);
        return view('socios.edit', [
            'socio' => $socio
        ]);
    }

    public function edit(Request $request, $id)
    {
        $result = $this->model->findOrFail($id);
        $result->update([
            'nome' => $request->input('nome'),
            'email' => $request->input('email'),
            'cpf' => $request->input('cpf'),
            'data_nascimento' => $request->input('data_nascimento'),
            'telefone' => $request->input('telefone')
        ]);

        return redirect()->action('SocioController@show', ['id' => $id])->with('status', 'Sócio editada com sucesso!');
    }

    public function delete($id)
    {
        $result = $this->model->where('id', '=', $id)->delete();
        return redirect()->action('SocioController@index')->with('status', 'Sócio deletada com sucesso!');
    }

    public function search(Request $request){
        $result = (($request->cpf === null || $request->cpf == '') ? $this->model->all() : $this->model->where('cpf','like',"%$request->cpf%")->orderBy('id')->get());
        return view('socios.index', [
            'socios' => $result
        ]);
    }
}
